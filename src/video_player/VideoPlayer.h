#ifndef __video_player_h__
#define __video_player_h__
#include <stdio.h>
#include <stdint.h>

/*
 * takes a stream of -1 seperated PPMs and plays them to an SDL window
 * with 'duration' time between each frame. brightness, contrast, saturation parameters
 * are used to alter how to video looks.
 *
 * returns a bool; 0 = error, 1 = successful video playthrough
 */
int playVideo(FILE *stream, uint32_t duration, int brightness, int contrast, int saturation);

#endif
