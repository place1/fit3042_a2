#include <stdbool.h>
#include <stdio.h>
#include <SDL2/SDL.h>
#include "RenderFrame.h"
#include "ReadHeader.h"
#include "rendering_middleware/Filter.h"
#include "rendering_middleware/BrightnessFilter.h"
#include "rendering_middleware/ContrastFilter.h"
#include "rendering_middleware/SaturationFilter.h"

void cleanupResources(SDL_Surface *surface, SDL_Renderer *renderer, SDL_Texture *texture);

bool renderFrame(SDL_Window **window, SDL_Renderer **renderer, FILE *stream, RenderingOptions options) {
	// read frame header
	VideoHeader header = {};
	if (!readHeader(stream, &header)) {
		fprintf(stderr, "Error reading frame header from stream\n");
		return false;
	}

	// create the window and renderer if they haven't been created already
	if (*window == NULL) {
		if ((*window = SDL_CreateWindow("ppm player", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, header.width, header.height, 0)) == NULL) {
			fprintf(stderr, "failed to create SDL window: %s\n", SDL_GetError());
			return false;
		}
	}
	if (*renderer == NULL) {
		if ((*renderer = SDL_CreateRenderer(*window, -1, SDL_RENDERER_ACCELERATED)) == NULL) {
			fprintf(stderr, "failed to create SDL renderer %s\n", SDL_GetError());
			return false;
		}
	}

	// create the SDL surface for the frame
	const int bitDepth = 8;
	const int channels = 4;
	const int depth = channels * bitDepth;
	Uint32 rmask, gmask, bmask, amask;
	#if SDL_BYTEORDER == SDL_BIG_ENDIAN
		rmask = 0xff000000;
		gmask = 0x00ff0000;
		bmask = 0x0000ff00;
		amask = 0x000000ff;
	#else
		rmask = 0x000000ff;
		gmask = 0x0000ff00;
		bmask = 0x00ff0000;
		amask = 0xff000000;
	#endif

	SDL_Surface *surface = SDL_CreateRGBSurface(0, header.width, header.height, depth, rmask, gmask, bmask, amask);
	if (surface == NULL) {
		fprintf(stderr, "error creating surface: %s\n", SDL_GetError());
		cleanupResources(NULL, *renderer, NULL);
		return false;
	}

	size_t dataLength = header.height * header.width * channels;
	for (size_t i = 0; i < dataLength; i += 4) {
		unsigned char pixelData[3];
		if (fread(&pixelData, sizeof(unsigned char), 3, stream) != 3) {
			fprintf(stderr, "Error while reading frame data. Unexpected EOF\n");
			cleanupResources(surface, *renderer, NULL);
			return false; // indicate unexpected EOF
		}
		else {
			((unsigned char *)(surface->pixels))[i] = pixelData[0];
			((unsigned char *)(surface->pixels))[i+1] = pixelData[1];
			((unsigned char *)(surface->pixels))[i+2] = pixelData[2];
			((unsigned char *)(surface->pixels))[i+3] = 255;
		}
	}

	// apply filters
	applyFilter(getBrightnessFilter(&surface, options.brightness));
	applyFilter(getContrastFilter(&surface, options.contrast));
	applyFilter(getSaturationFilter(&surface, options.saturation));

	// check the the frame ended with -1
	int delimiter = 0;
	int matches = fread(&delimiter, sizeof(int), 1, stream);
	if (!feof(stream) && (delimiter != -1 || matches == 0)) {
		fprintf(stderr, "error: end of frame delimiting character wasn't -1. Num matches: %i, Matched Char: '%c'\n", matches, delimiter);
		cleanupResources(surface, *renderer, NULL);
		return false;
	}

	// create SDL Texture
	SDL_Texture *texture = SDL_CreateTextureFromSurface(*renderer, surface);
	if (texture == NULL) {
		fprintf(stderr, "failed to create SDL texture: %s\n", SDL_GetError());
		cleanupResources(surface, *renderer, NULL);
		return false;
	}
	SDL_FreeSurface(surface); // free the surface now that we've created the texture from it

	// render the frame
	if (SDL_RenderClear(*renderer) < 0) {
		fprintf(stderr, "failed to clear renderer: %s\n", SDL_GetError());
		cleanupResources(surface, *renderer, texture);
		return false;
	}
	if (SDL_RenderCopy(*renderer, texture, NULL, NULL) < 0) {
		fprintf(stderr, "failed to copy texture into renderer: %s\n", SDL_GetError());
		cleanupResources(surface, *renderer, texture);
		return false;
	}
	SDL_RenderPresent(*renderer);

	// cleanupResources the texture
	SDL_DestroyTexture(texture);

	return true;
}

void cleanupResources(SDL_Surface *surface, SDL_Renderer *renderer, SDL_Texture *texture) {
	if (surface) {
		SDL_FreeSurface(surface);
	}
	if (renderer) {
		SDL_DestroyRenderer(renderer);
	}
	if (texture) {
		SDL_DestroyTexture(texture);
	}
}
