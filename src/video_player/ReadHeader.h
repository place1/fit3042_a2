#ifndef __header_reader_h__
#define __header_reader_h__
#include <stdbool.h>
#include <stdio.h>

/*
 * A structure to represent the important information in
 * a PPM header
 */
typedef struct VideoHeader {
	size_t width;
	size_t height;
	unsigned int colorDepth;
} VideoHeader;

/*
 * This function reads a PPM header from the input stream and assigned important
 * values to the supplied 'header'
 *
 * returns a boolean, 0 == error, 1 == success
 */
bool readHeader(FILE *stream, VideoHeader *header);

#endif
