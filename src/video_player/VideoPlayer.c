
#include <stdbool.h>
#include <stdlib.h>
#include <stdint.h>
#include <SDL2/SDL.h>
#include "VideoPlayer.h"
#include "RenderFrame.h"
#include "Events.h"

uint32_t addUserEventCallback(uint32_t interval, void *eventType);
void cleanup(SDL_Window *window, SDL_Renderer *renderer);
char fpeakc(FILE *stream);

int playVideo(FILE *stream, uint32_t duration, int brightness, int contrast, int saturation) {
	// Initialize SDL.
	if (SDL_Init(SDL_INIT_EVERYTHING) < 0) {
		fprintf(stderr, "failed to Initialize SDL: %s\n", SDL_GetError());
		return false;
	}

	RenderingOptions options = {
		.brightness = brightness,
		.contrast = contrast,
		.saturation = saturation
	};

	UserEvent renderFrameEvent = RENDER_FRAME;
	UserEvent streamFinishedEvent = STREAM_FINISHED;
	SDL_Window *win = NULL;
	SDL_Renderer *renderer = NULL;
	SDL_Event event;
	if (pushEvent(RENDER_FRAME) < 0) { // seed the event queue with a render frame event.
		fprintf(stderr, "Error pushing a RENDER_FRAME event\n");
		return false;
	}
	while (SDL_WaitEvent(&event)) { // main event loop
		switch (event.type) {
			case SDL_QUIT:
				cleanup(win, renderer);
				return true;
			case SDL_USEREVENT:
				switch (event.user.code) {
					case STREAM_FINISHED:
						cleanup(win, renderer);
						return true;
					case RENDER_FRAME:
						// create a timer that will push the render_frame event when it completes
						if (SDL_AddTimer(duration, &addUserEventCallback, &renderFrameEvent) == 0) {
							fprintf(stderr, "failted to add SDL timer %s\n", SDL_GetError());
							return false;
						}
						// if the stream is finished. Push a STREAM_FINISHED event after delay
						if ((int)fpeakc(stream) == EOF) {
							// add a timer to push STREAM_FINISHED to flag the end of the video
							if (SDL_AddTimer(duration, &addUserEventCallback, &streamFinishedEvent) == 0) {
								fprintf(stderr, "failed to add SDL timer: %s\n", SDL_GetError());
								return false;
							}
						}
						else {
							// if the stream isn't finished, then render a frame.
							if (!renderFrame(&win, &renderer, stream, options)) {
								fprintf(stderr, "Error rendering frame\n");
								return false;
							}
						}
						break;
				}
				break;
		}
	}
	return true;
}

// TODO: determine how to handle a failure in pushEvent when this may not run on the main thread.
// this method is used it push a UserEvent event onto the queue after a delay
// note: the userEvent argument is cast to a UserEvent as that's what is expected to be passed!!!
uint32_t addUserEventCallback(uint32_t interval, void *userEvent) {
	pushEvent(*(UserEvent *)userEvent);
	return 0; // the docs say to return interval but i've found that to behave incorrectly
}

void cleanup(SDL_Window *window, SDL_Renderer *renderer) {
	if (window) {
		SDL_DestroyWindow(window);
	}
	if (renderer) {
		SDL_DestroyRenderer(renderer);
	}
	SDL_Quit();
}

char fpeakc(FILE *stream) {
	if (feof(stream)) {
		return EOF;
	}
	char c = fgetc(stream);
	ungetc(c, stream);
	return c;
}
