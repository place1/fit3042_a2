#include <stdbool.h>
#include <stdio.h>
#include "ReadHeader.h"

bool readHeader(FILE *stream, VideoHeader *header) {
	int matches = 0;
	if ((matches = fscanf(stream, "P6 %zd %zd %d ", &(header->width), &(header->height), &(header->colorDepth))) != 3) {
		if (matches == EOF) {
			fprintf(stderr, "Error. Could not read header from input stream. Unexpected EOF\n");
		}
		else {
			fprintf(stderr, "Error. Could not read the header from the input stream: matched %i/3 required header fields\n", matches);
		}
		return false;
	}
	return true;
}
