#ifndef __video_player_events_h__
#define __video_player_events_h__

/*
 * An enum to represent custom types of events
 * RENDER_FRAME represents an event that should be processed
 * by rendering a frame
 * STREAM_FINISHED represents an event that should be processed
 * by ending the consumption of a stream.
 *
 * These event types are used in the main event loop in VideoPlayer
 */
typedef enum {
	RENDER_FRAME,
	STREAM_FINISHED
} UserEvent;

/*
 * This is an abstraction for SDL_PushEvent.
 * This method is used to push a UserEvent onto the SDL Event Queue
 *
 * returns a negitive value on failure
 */
int pushEvent(UserEvent event);

#endif
