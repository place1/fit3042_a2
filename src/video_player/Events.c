#include "Events.h"
#include <SDL2/SDL.h>

/*
 * returns a negitive value on failure
*/
int pushEvent(UserEvent event) {
	SDL_Event sdlEvent;
	SDL_UserEvent userevent;

	userevent.type = SDL_USEREVENT;
	userevent.code = event;

	sdlEvent.type = SDL_USEREVENT;
	sdlEvent.user = userevent;

	int result = SDL_PushEvent(&sdlEvent);
	if (result < 0) {
		fprintf(stderr, "Error pusing event onto SDL event queue: %s\n", SDL_GetError());
	}
	return result;
}
