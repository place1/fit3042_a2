#include <math.h>
#include "BrightnessFilter.h"
#include "Filter.h"
#include "../../util/RGBA_To_HSL.h"
#include "../../util/general.h"

void brightnessFilterMiddleware(SDL_Surface **surface, int param) {
	// apply filter to the surface
	if (param == 50) { // 50 means we don't want to change anything
		return;
	}

	SDL_Surface *_surface = *surface; // just a convenience reference

	int bFactor = (double)((param-50)*2)/100 * 255;
	for (int i = 0; i < _surface->w * _surface->h * 4; i += 4) {
		unsigned char red = ((unsigned char *)(_surface->pixels))[i];
		unsigned char green = ((unsigned char *)(_surface->pixels))[i+1];
		unsigned char blue = ((unsigned char *)(_surface->pixels))[i+2];

		red = limit(bFactor + (int)red, 0, 255); // limit function just makes sure the result of the calculation is rounded correctly to 0 or 255 if it's out of range
		green = limit(bFactor + (int)green, 0, 255);
		blue = limit(bFactor + (int)blue, 0, 255);

		((unsigned char *)(_surface->pixels))[i] = red;
		((unsigned char *)(_surface->pixels))[i+1] = green;
		((unsigned char *)(_surface->pixels))[i+2] = blue;
	}
}

Filter getBrightnessFilter(SDL_Surface **surface, int param) {
	Filter filter = {
		.param = param,
		.surface = surface,
		.filterMiddleware = &brightnessFilterMiddleware
	};

	return filter;
}
