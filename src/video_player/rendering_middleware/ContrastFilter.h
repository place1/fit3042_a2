#ifndef __contrast_filter_h
#define __contrast_filter_h
#include <SDL2/SDL.h>
#include "Filter.h"

/*
 * Creates a Filter structure for applying a Contrast Filter.
 * 'surface' is a reference to the SDL_Surface that the filter should modify
 * 'param' is an integer parameter that the filter function can use to control
 * the filtering behaviour.
 *
 * returns a Filter structure
 */
Filter getContrastFilter(SDL_Surface **surface, int param);

#endif
