#include <SDL2/SDL.h>
#include <math.h>
#include "Filter.h"
#include "SaturationFilter.h"
#include "../../util/RGBA_To_HSL.h"

void saturationFilterMiddleware(SDL_Surface **surface, int param) {
	// apply filter to the surface
	if (param == 50) { // 50 means we don't want to change anything
		return;
	}

	SDL_Surface *_surface = *surface;

	// do the actual surface pixel manipulation to apply filter effect
	for (int i = 0; i < _surface->w * _surface->h * 4; i += 4) {
		RGBPixel rgb = {
			.R = ((unsigned char *)(_surface->pixels))[i],
			.G = ((unsigned char *)(_surface->pixels))[i+1],
			.B = ((unsigned char *)(_surface->pixels))[i+2]
		};
		HSLPixel hsl = hsl_from_rgb(rgb);
		hsl.S = hsl.S * pow(((double)param)/50, 1.5);
		rgb = rgb_from_hsl(hsl);
		((unsigned char *)(_surface->pixels))[i] = rgb.R;
		((unsigned char *)(_surface->pixels))[i+1] = rgb.G;
		((unsigned char *)(_surface->pixels))[i+2] = rgb.B;
	}
}

Filter getSaturationFilter(SDL_Surface **surface, int param) {
	Filter filter = {
		.param = param,
		.surface = surface,
		.filterMiddleware = &saturationFilterMiddleware
	};

	return filter;
}
