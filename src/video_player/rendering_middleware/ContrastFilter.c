#include <SDL2/SDL.h>
#include "Filter.h"
#include "ContrastFilter.h"
#include "../../util/RGBA_To_HSL.h"
#include "../../util/general.h"

void contrastFilterMiddleware(SDL_Surface **surface, int param) {
	// apply filter to the surface
	if (param == 50) { // 50 means we don't want to change anything
		return;
	}

	SDL_Surface *_surface = *surface;

	// do the actual surface pixel manipulation to apply filter effect
	double adjustedParam = (double)((param-50)*2)/100 * 128;
	double cFactor = (259 * (adjustedParam + 255)) / (255 * (259 - adjustedParam));
	for (int i = 0; i < _surface->w * _surface->h * 4; i += 4) {
		unsigned char red = ((unsigned char *)(_surface->pixels))[i];
		unsigned char green = ((unsigned char *)(_surface->pixels))[i+1];
		unsigned char blue = ((unsigned char *)(_surface->pixels))[i+2];

		red = (unsigned char)flimit(cFactor * (double)((double)red - 128) + 128, 0, 255); // flimit function just makes sure the result of the calculation is rounded correctly to 0 or 255 if it's out of range
		green = (unsigned char)flimit(cFactor * (double)((double)green - 128) + 128, 0, 255);
		blue = (unsigned char)flimit(cFactor * (double)((double)blue - 128) + 128, 0, 255);

		((unsigned char *)(_surface->pixels))[i] = red;
		((unsigned char *)(_surface->pixels))[i+1] = green;
		((unsigned char *)(_surface->pixels))[i+2] = blue;
	}
}

Filter getContrastFilter(SDL_Surface **surface, int param) {
	Filter filter = {
		.param = param,
		.surface = surface,
		.filterMiddleware = &contrastFilterMiddleware
	};

	return filter;
}
