#ifndef __render_frame_h__
#define __render_frame_h__
#include <stdbool.h>
#include <stdio.h>
#include <SDL2/SDL.h>

/*
 * A structure to represent valid the parameters to the
 * filtering options
 */
typedef struct RenderingOptions {
	int brightness;
	int contrast;
	int saturation;
} RenderingOptions;

/*
 * this function will render a PPM from the inputstream using RenderingOptions to modify
 * how it looks.
 * The frame will be rendered to the supplied window, with the supplied renderer.
 * 'window' and 'renderer' are optional (you can pass NULL).
 * If 'window' or 'renderer' are passed in as NULL then this function will create a
 * window and renderer to use and assign them to the argument.
 *
 * returns a boolean, 0(false) = errors, 1(true) = success
 */
bool renderFrame(SDL_Window **window, SDL_Renderer **renderer, FILE *stream, RenderingOptions options);

#endif
