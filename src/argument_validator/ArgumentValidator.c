#include <stdio.h>
#include <stdlib.h>
#include "ArgumentValidator.h"

bool validateArguments(int argc, const char *argv[], ValidArguments *va) {
	if (argc != 5) { // program, delay, brightness, contrast, saturation
		fprintf(stderr, "Invalid number of arguments\n");
		return false;
	}

	va->delay = atoi(argv[1]); // set the delay argument

	// read brightness, contrast and saturation from argv and set them into ValidArguments structure
	for (int i = 2; i < argc; i++) {
		int value = atoi(argv[i]);
		if (value < 0 || value > 100) { // validate value
			fprintf(stderr, "Invalid value for argument: Brightness, Contrast and Saturation must be between 0 and 100 inclusive.\n");
			return false;
		}
		switch(i) {
			case 2:
				va->brightness = value;
				break;
			case 3:
				va->contrast = value;
				break;
			case 4:
				va->saturation = value;
				break;
		}
	}

	return true;
}
