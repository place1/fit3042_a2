#ifndef __argument_validator_h__
#define __argument_validator_h__
#include <stdint.h>
#include <stdbool.h>

/*
 * A structure to represent the validated arguments of the program
 */
typedef struct ValidArguments {
	uint32_t delay;
	int brightness;
	int contrast;
	int saturation;
} ValidArguments;

/*
 * This function takes argc and argv (most likely directly from the main function) and
 * validates the contained arguments for the program and, if successfully validated, will
 * store them in the supplied ValidArguments structure.
 *
 * returns a boolean, 0 == arguments were not valid, 1 == arguments successfully validated
 */
bool validateArguments(int argc, const char *argv[], ValidArguments *va);

#endif
