#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdint.h>
#include <signal.h>
#include "video_player/VideoPlayer.h"
#include "argument_validator/ArgumentValidator.h"

void sigHandler(int sig);
void printUsage();

int main(int argc, const char* argv[]) {
	// set up a listener for the interrupt signal
	ValidArguments args = {};
	if (!validateArguments(argc, argv, &args)) { // if the arguments aren't valid
		printUsage();
		exit(1);
	}

	if (signal(SIGINT, sigHandler) == SIG_ERR) {
		fprintf(stderr, "can't catch SIGINT\n");
		exit(1);
	}

	bool result = playVideo(stdin, args.delay, args.brightness, args.contrast, args.saturation);

	return !result; // play video will return 'true' to indicate success and false otherwise.
	                // 'not' result to return with the correct status
}

void sigHandler(int sig) {
	if (sig == SIGINT) {
		printf("exiting...\n");
		exit(0);
	}
}

void printUsage() {
	printf("Usage:\n"
	       "arg 1: delay (positive int)\n"
	       "arg 2: brightness (int in range [0, 100])\n"
	       "arg 3: contrast (int in range [0, 100])\n"
	       "arg 4: saturation (int in range [0, 100])\n");
}
