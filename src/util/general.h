#ifndef __general_util_h__
#define __general_util_h__

/*
 * This module contains general utility methods that are
 * available for this application
 */

/*
 * This function takes a double, a minimum value and maximum value
 * and sets the input double to be within the range of min and max inclusive
 *
 * returns a double that is equal to x, or max if x > max, or min if x < min
 */
double flimit(double x, double min, double max);

/*
 * This function takes a integer, a minimum value and maximum value
 * and sets the input integer to be within the range of min and max inclusive
 *
 * returns a integer that is equal to x, or max if x > max, or min if x < min
 */
int limit(int x, int min, int max);

#endif
