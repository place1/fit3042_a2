#ifndef __rgba_to_hsl_h__
#define __rgba_to_hsl_h__

/*
 * This structure represents an HSL colourspace Pixel
 */
typedef struct HSLPixel {
	double H;
	double S;
	double L;
} HSLPixel;

/*
 * This structure represents an RGB colourspace Pixel
 */
typedef struct RGBPixel {
	unsigned char R;
	unsigned char G;
	unsigned char B;
} RGBPixel;

/*
 * This function converts a RGB pixel to an HSL pixel
 */
HSLPixel hsl_from_rgb(RGBPixel rgb);

/*
 * This function converts an HSL pixel to an RGB pixel
 */
RGBPixel rgb_from_hsl(HSLPixel hsl);

#endif
