#include "general.h"

double flimit(double x, double min, double max) {
	if (x > max) {
		return max;
	}
	if (x < min) {
		return min;
	}
	return x;
}

int limit(int x, int min, int max) {
	if (x > max) {
		return max;
	}
	if (x < min) {
		return min;
	}
	return x;
}
