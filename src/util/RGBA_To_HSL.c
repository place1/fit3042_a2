#include <math.h>
#include <stdio.h>
#include "RGBA_To_HSL.h"

HSLPixel hsl_from_rgb(RGBPixel p) {
	HSLPixel hsl = {};
	double _r = (double)p.R/255;
	double _g = (double)p.G/255;
	double _b = (double)p.B/255;

	double cmax = fmax(_r, fmax(_g, _b));
	double cmin = fmin(_r, fmin(_g, _b));
	double delta = cmax - cmin;

	// calculate Hue
	if (delta == 0) {
		hsl.H = 0;
	}
	else {
		if (cmax == _r) {
			hsl.H = 60 * fmod((_g - _b) / delta, 6.0);
		}
		else if (cmax == _g) {
			hsl.H = 60 * ((_b - _r) / delta + 2);
		}
		else if (cmax == _b) {
			hsl.H = 60 * ((_r - _g) / delta + 4);
		}
	}

	if (hsl.H < 0) {
		hsl.H = 360 + hsl.H;
	}

	// calculate Luminance
	hsl.L = (cmax + cmin) / 2;

	// calculate Saturation
	if (delta == 0) {
		hsl.S = 0;
	}
	else {
		hsl.S = delta / (1 - fabs(2 * hsl.L - 1));
	}

	return hsl;
}

RGBPixel rgb_from_hsl(HSLPixel hsl) {
	RGBPixel rgb = {};

	double c = (1 - fabs(2 * hsl.L - 1)) * hsl.S;
	double x = c * (1 - fabs(fmod(hsl.H / 60, 2) - 1));
	double m = hsl.L - c / 2;

	double _r = 0;
	double _g = 0;
	double _b = 0;
	if (hsl.H/60 < 1) {
		_r = c;
		_g = x;
		_b = 0;
	}
	else if (hsl.H/60 < 2) {
		_r = x;
		_g = c;
		_b = 0;
	}
	else if (hsl.H/60 < 3) {
		_r = 0;
		_g = c;
		_b = x;
	}
	else if (hsl.H/60 < 4) {
		_r = 0;
		_g = x;
		_b = c;
	}
	else if (hsl.H/60 < 5) {
		_r = x;
		_g = 0;
		_b = c;
	}
	else if (hsl.H/60 <= 6) {
		_r = c;
		_g = 0;
		_b = x;
	}

	rgb.R = fmax(fmin(_r + m, 1) * 255, 0);
	rgb.G = fmax(fmin(_g + m, 1) * 255, 0);
	rgb.B = fmax(fmin(_b + m, 1) * 255, 0);

	return rgb;
}

// int error(int a, int b, int margin) {
// 	if (a-margin <= b && a+margin >= b) {
// 		return 1;
// 	}
// 	else {
// 		return 0;
// 	}
// }
//
// int derror(double a, double b, double margin) {
// 	if (a-margin <= b && a+margin >= b) {
// 		return 1;
// 	}
// 	else {
// 		return 0;
// 	}
// }
//
// int main() {
// 	for (int x = 0; x <= 255; x++) {
// 		for (int y = 0; y <= 255; y++) {
// 			for (int z = 0; z <= 255; z++) {
// 				RGBPixel rgb = {
// 					.R = x,
// 					.G = y,
// 					.B = z
// 				};
//
// 				HSLPixel hsl = hsl_from_rgb(rgb);
//
// 				RGBPixel _rgb = rgb_from_hsl(hsl);
//
// 				int margin = 50;
// 				if (!error(rgb.R, _rgb.R, margin) || !error(rgb.G, _rgb.G, margin) || !error(rgb.B, _rgb.B, margin)) {
// 					fprintf(stderr, "I: %i %i %i\nP: %i %i %i\n\n", rgb.R, rgb.G, rgb.B, _rgb.R, _rgb.G, _rgb.B);
// 					return 1;
// 				}
// 			}
// 		}
// 	}
//
//
// 	for (int x = 0; x <= 360; x++) {
// 		for (int y = 0; y <= 100; y++) {
// 			for (int z = 0; z <= 100; z++) {
// 				HSLPixel hsl = {
// 					.H = (double)x,
// 					.S = (double)y/100,
// 					.L = (double)z/100
// 				};
//
// 				RGBPixel rgb = rgb_from_hsl(hsl);
//
// 				HSLPixel _hsl = hsl_from_rgb(rgb);
//
// 				int margin = 400;
// 				if (!derror(hsl.H, _hsl.H, margin) || !derror(hsl.S, _hsl.S, margin) || !derror(hsl.L, _hsl.L, margin)) {
// 					fprintf(stderr, "I: %f %f %f\nM: %i %i %i\nP: %f %f %f\n\n", hsl.H, hsl.S, hsl.L, rgb.R, rgb.G, rgb.B, _hsl.H, _hsl.S, _hsl.L);
// 					return 1;
// 				}
// 			}
// 		}
// 	}
//
//
// 	return 0;
// }
