CC=gcc
CCFLAGS=-Wall -std=c99
LDFLAGS=-lm `sdl2-config --cflags --libs`
SDIR=src
ODIR=bin
SOURCES=$(shell find $(SDIR) -name "*.c")
OBJECTS=$(SOURCES:.c=.o)
TARGET=ppmplayer

.PHONY: all clean debug partialClean setDebug

all: $(TARGET) partialClean

debug: setDebug $(TARGET)

# make the program
$(TARGET): $(OBJECTS)
	-mkdir $(ODIR)
	$(CC) -o $(ODIR)/$@ $^ $(LDFLAGS)

%.o: %.c %.h
	$(CC) $(CCFLAGS) -c $< -o $@ $(LDFLAGS)

%.o: %.c
	$(CC) $(CCFLAGS) -c $< -o $@ $(LDFLAGS)

partialClean:
	@find $(SDIR)/ -type f -name '*.o' -delete

clean: partialClean
	@rm -f -r $(ODIR)/

setDebug:
	$(eval CCFLAGS := -g $(CCFLAGS))
