# FIT3042 Assignment 2 - PPM Player

## James Batt

### Requirements
* SDL2.0.4 installed at the system level.

### How to build and run
* run `make` from the command line at the root of the project.
* run `./bin/ppmplayer` to run the program.
	* the program required 4 arguments.
		1. delay
		2. brightness [0, 100]
		3. contrast [0, 100]
		4. saturation [0, 100]

* I've included and OS X version of Robyn's 'rledecode' and a Linux version which can be used to decode an RLE file and pipe it into the 'ppmplayer'

* All required functionality is implemented

### How the event loop works:
* When a RENDER_FRAME event is processed it will
	1. add a timer which on completion pushed another RENDER_FRAME event
	2. check if the stream is finished. If the stream is finished it will add a timer that will push a STREAM_FINISHED event.
	3. if the stream isn't finished, we'll call to render a frame. The frame rendering happens on the Event loop thread.


* I add a timer to push the next RENDER_FRAME event before rendering the current frame in order to make sure the timing of frames is kept tight. I don't want to add a timer after the frame render because then the delay between frames would be equal to the Delay parameter plus the rendering time. Adding the timer before rendering means the timer can start counting down during the rendering computation.


* By rendering on the same thread as the event loop i can ensure that rendering frames are serialised in the case that the machine can't render frames fast enough and so the event queue will pend the RENDER_FRAME events which it can't process fast enough.

## resources

* Brightness filter manipulation was sourced from here http://www.dfstudios.co.uk/articles/programming/image-programming-algorithms/image-processing-algorithms-part-4-brightness-adjustment/
* Contrast image manipulation was sourced from here http://www.dfstudios.co.uk/articles/programming/image-programming-algorithms/image-processing-algorithms-part-5-contrast-adjustment/
* HSL colour conversion for the Saturation filter was sourced from:
	* rgb -> hsl http://www.rapidtables.com/convert/color/rgb-to-hsl.htm
	* hsl -> rgb http://www.rapidtables.com/convert/color/hsl-to-rgb.htm
